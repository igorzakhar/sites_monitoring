com = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

net = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

org = {
    'expiration_date': r'Registry Expiry Date:\s?(?P<date>.+)'
}

uk = {
    'expiration_date': r'Renewal date:\s*(?P<date>.+)'
}

pl = {
    'expiration_date': r'Renewal date:\s*(?P<date>.+)'
}

ru = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

su = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

jp = {
    'expiration_date': r'\[Expires on\]\s?(?P<date>.+)',
}

de = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

at = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

eu = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

biz = {
    'expiration_date': r'Domain Expiration Date:\s?(?P<date>.+)',
}

info = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)',

}

name = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

us = {
    'expiration_date': r'Expiration Date:\s?(?P<date>.+)'
}

co = {
    'expiration_date': r'Domain Expiration Date:\s?(?P<date>.+)',
}

me = {
    'expiration_date': r'Domain Expiration Date:\s?(?P<date>.+)',
}

be = {
        'expiration_date': r'Renewal date:\s*(?P<date>.+)'
}

nz = {
    'expiration_date': r'domain_datebilleduntil:\s?(?P<date>.+)',
}

cz = {
    'expiration_date': r'expire:\s?(?P<date>.+)',
}

it = {
    'expiration_date': r'Expire Date:\s?(?P<date>.+)',
}

fr = {
    'expiration_date': r'Expiry Date:\s?(?P<date>.+)',
}

xnp1ai = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

xnd1acj3b = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

xn80aswg = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

xn80asehdb = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

xn80adx = {
    'expiration_date': r'\npaid-till:\s*(?P<date>.+)'
}

