import asyncio
from datetime import datetime, timezone
import re
from urllib.parse import urlparse

from aiohttp import ClientSession
import dateutil.parser

import regex_map


def load_urls4check(path):
    with open(path, mode='r') as fp:
        urls = fp.read()
        urls = urls.rstrip().split('\n')
        return urls


async def fetch_response_status(url, session):
    async with session.head(url) as response:
        return response.status


async def whois_me(domain):
    domain = urlparse(domain).netloc
    process = await asyncio.create_subprocess_exec(
        *['whois', domain],
        stdout=asyncio.subprocess.PIPE
    )
    result, _ = await process.communicate()
    return result.decode('utf-8')


async def get_status_for_each_url(domain, session):
    http_status = await fetch_response_status(domain, session)
    whois_info = await whois_me(domain)
    return {
        'domain': domain,
        'http_status': http_status,
        'whois_info': whois_info
    }


async def check_sites_health(domains_list, loop):
    async with ClientSession(loop=loop) as session:
        tasks = [
            get_status_for_each_url(domain, session)
            for domain in domains_list
        ]
        return await asyncio.gather(*tasks)


def parse_expiry_date(domain, whois_response):
    maxsplit = 2
    domain_chunks = domain.split('.', maxsplit)
    top_level_domain = domain_chunks[-1]
    pattern = getattr(regex_map, top_level_domain)
    expiration_date = re.search(
        pattern['expiration_date'],
        whois_response,
        re.IGNORECASE
    )
    expiration_date = expiration_date.group('date')
    return dateutil.parser.parse(expiration_date)


def print_results(check_sites_results):
    date_now = datetime.now(timezone.utc)
    status_string = '{:20s}: status code: {}, days until expiry: {} days ({})'
    for check_site in check_sites_results:
        expiry_date = parse_expiry_date(
            check_site['domain'],
            check_site['whois_info']
        )
        days_until_expiry = expiry_date - date_now
        print(status_string.format(
            check_site['domain'],
            check_site['http_status'],
            days_until_expiry.days,
            expiry_date.date()
        ))


def main(urls):
    loop = asyncio.get_event_loop()
    results = loop.run_until_complete(check_sites_health(urls, loop))
    print_results(results)
    loop.close()


if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        try:
            urls = load_urls4check(sys.argv[1])
            main(urls)
        except OSError as err:
            print(err)
    else:
        print("Usage: python3 check_sites_health.py <path/to/file/with/urls>")
